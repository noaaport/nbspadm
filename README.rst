Usage
=====

  nbspupdate [-a <osname>-<osarch>] [-c] [-k] [-q] [-u] [-F] [<pkgname>]

If <pkgname> is not specified the default is **nbsp**.

Options
=======

* -a => Use the given <osname>-<osarch>, otherwise the hosts's parameters
* -c => Just check, don't download package
* -k => If -u is given, keep (do not delete) the package file 
* -q => Quiet (default is verbose, unless set otherwise in the conf file)
* -u => Upgrade (pkg_delete and pkg_add)
* -F => Force download (or install if -u is given ) of pkg unconditionally

Description
===========

**Nbsp** can be upgraded easily. The first thing is to install the nbspadm
package for your OS. That package contains the program nbspupdate
which is used as follows. Executing

  nbspupdate

without arguments, will check the version of the installed Nbsp package
against the latest one available, and if the latter is newer than
the former it will download it. The Nbsp package can then be upgraded
manually as usual. Alternatively, executing

  nbspupdate -u

will do the upgrade automatically after downloading the package as above.
Passing the -F option as well, i.e.,

  nbspupdate -u -F

*Nbspupdate* will do as above, even if the installed version is the same
as the latest one, or if Nbsp had not been installed before.
Nbspupdate can also upgrade Npemwin and Nbspadm itself, by passing
any of those names as an argument; i.e.,

  nbspupdate -u npemwin

will download and upgrade Npemwin if the latest version is different
than the installed one.
