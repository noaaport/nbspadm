#!%TCLSH%
#
# $Id$
#
# In the case of deb and rpm packages, the version string output here
# does not include the arch portion (this is also the convention used
# in nbspupdate). In the rpm's, we also cut the ".el<n>" portion. 
#
# The version string output here includes the package build number.

# Default, unless overriden in the cmd line
set pkgname "nbspadm";

proc nbspadmversion_get_pkgversion {pkgname} {

    if {[file executable "/usr/local/sbin/pkg"]} {
	set output [exec pkg info zip | grep Version];
	set version [string trim [lindex [split $output ":"] 1]];
    } elseif {[file executable "/usr/bin/dpkg"]} {
	set output [exec dpkg -s ${pkgname} | grep -m 1 "Version"];
	set version [string trim [lindex [split $output] 1]];
    } elseif {[file executable "/bin/rpm"]} {
	set output [exec rpm -qa | grep "${pkgname}-"];
	regexp ${pkgname}-(.+) $output match version;
	# Cut the "el6.x86_64" portion
	set version [file rootname [file rootname $version]];
    } else {
	set version "unknown";
    }

    return $version;
}

#
# main
#

if {$argc != 0} {
    set version_list [list];
    foreach pkgname $argv {
	lappend version_list [nbspadmversion_get_pkgversion $pkgname];
    }
    puts [join $version_list "|"];
} else {
    puts [nbspadmversion_get_pkgversion $pkgname];
}
