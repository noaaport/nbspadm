#!/bin/sh
#
# $Id$
#

# read name and version
. ../../VERSION

bzr export ${name}-${version}.tgz
