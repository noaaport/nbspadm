#!/bin/sh

project=nbspadm
#
masterhost="bzr+ssh://repo.1-loop.net"
masterrepo="${masterhost}/home/repo/bzr/noaaport"

bzr branch ${masterrepo}/${project}/trunk ${project}
