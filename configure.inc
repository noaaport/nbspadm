#
# $Id$
#

# defaults (FreeBSD)
INCLUDE=".include"
Q='"'
PREFIX=usr/local
CONFDIR='${DESTDIR}/etc'   # or '${PKGBUILDDIR}/etc'
INSTALL=install

TCLSH=/usr/local/bin/tclsh8.6

CC=cc
CCWFLAGS="-Wall -Wextra"
SUFFIXRULES=".c.o:"

# excude options to tar
EXCLUDE="--exclude "

os=`uname`
[ $os = Linux ] && flavor=unknown
if [ -f /etc/fedora-release ]
then
    flavor=fedoracore
elif [ -f /etc/SuSE-release ] 
then
   flavor=opensuse
elif [ -f /etc/redhat-release ] 
then
   flavor=centos
elif [ -f /etc/debian_version ] 
then
   flavor=debian
fi

case $os in
    FreeBSD) 
        # use the defaults
        ;;
    Linux)
	INCLUDE=include
	Q=
	# PREFIX=usr
	# CONFDIR='${PKGBUILDDIR}/etc'

	TCLSH=/usr/bin/tclsh
	SUFFIXRULES="%.o: %.c"
	EXCLUDE="--exclude "

    	if [ $flavor = opensuse ]
	then
	   continue
	elif [ $flavor = centos ]
	then
	   continue
	elif [ $flavor = debian ]
	then
	   TCLSH=/usr/bin/tclsh8.5
    	fi
     	;;
esac

configure_default () {

  makefile_out=Makefile
  [ $# -eq 1 ] && makefile_out=$1
  makefile_in=${makefile_out}.in

  sed \
      -e "/@include@/ s||$INCLUDE|" \
      -e "/@q@/ s||$Q|g" \
      -e "/@PREFIX@/ s||$PREFIX|" \
      -e "/@CONFDIR@/ s||$CONFDIR|" \
      -e "/@INSTALL@/ s||$INSTALL|" \
      -e "/@TCLSH@/ s||$TCLSH|" \
      -e "/@CC@/ s||$CC|" \
      -e "/@CCWFLAGS@/ s||$CCWFLAGS|" \
      -e "/@SUFFIXRULES@/ s||$SUFFIXRULES|" \
      -e "/@EXCLUDE@/ s||$EXCLUDE|" \
      $makefile_in > $makefile_out
}
